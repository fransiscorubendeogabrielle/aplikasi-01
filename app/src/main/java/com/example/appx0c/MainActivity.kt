package com.example.appx0c

import android.content.Intent
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_main.*
import mumayank.com.airlocationlibrary.AirLocation

class MainActivity : AppCompatActivity(), OnMapReadyCallback, View.OnClickListener {
    var airLoc : AirLocation? = null
    var gmap : GoogleMap? = null
    lateinit var mapFragment: SupportMapFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mapFragment = supportFragmentManager.findFragmentById(R.id.fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fab.setOnClickListener(this)


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onMapReady(p0: GoogleMap?) {
        gmap = p0
        if(gmap!=null){
            airLoc = AirLocation( this, true, true,
                object : AirLocation.Callbacks{
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(this@MainActivity, "gagal mendapatkan posisi",
                        Toast.LENGTH_SHORT).show()
                        editText.setText("gagal mendapatkan posisi")
                    }

                    override fun onSuccess(location: Location) {
                        val ll = LatLng(location.latitude,location.longitude)
                        gmap!!.addMarker(MarkerOptions().position(ll).title("Posisi Saya"))
                        gmap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                        editText.setText("Posisi Saya : LAT=${location.latitude}, " +
                        "LNG=${location.longitude}")

                    }
                })

        }
    }

    override fun onClick(v: View?) {
        airLoc = AirLocation( this, true, true,
            object : AirLocation.Callbacks{
                override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                    Toast.makeText(this@MainActivity, "gagal mendapatkan posisi",
                        Toast.LENGTH_SHORT).show()
                    editText.setText("gagal mendapatkan posisi")
                }

                override fun onSuccess(location: Location) {
                    val ll = LatLng(location.latitude,location.longitude)
                    gmap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                    editText.setText("Posisi Saya : LAT=${location.latitude}, " +
                            "LNG=${location.longitude}")

                }
            })
    }
}
